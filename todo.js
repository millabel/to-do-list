function aufgabeErledigen() {
	var checkboxId = this.id.replace("checkbox_", "");
	//alert(checkboxId)
	var neueAufgabeText = document.getElementById("element_" + checkboxId);

	//neueAufgabeText.innerText = "CLICKED"; // Testen, ob man auf das richtige Element zugreift

	if (this.checked) {
		neueAufgabeText.style.textDecoration = "line-through";
	}
	else {
		neueAufgabeText.style.textDecoration = "none";
	}
	
}

function editierenElement (){
	//this = btn.Edit
	var btnEditId = this.id.replace("btnEdit_", "");
	var editText = document.getElementById("element_" + btnEditId);
	
	var neuerText = prompt("Gegen Sie den neuen Text ein:")

	if(neuerText == false || neuerText == "" || neuerText == " ") {
		return false;
	}

	editText.innerText = neuerText;
}

function loeschenElement (){
	//this = btnLoeschen
	var btnLoeschenId = this.id.replace("btnLoeschen_", "");
	//alert(btnLoeschenId)

	document.getElementById("li_" + btnLoeschenId).style.display = "none";
}

function neueAufgabeHinzufuegen(liste, neueAufgabeText) {
	//alert ("To-Do: Neues Element hinzufügen");
	anzahlElemente++;
	
	var neuesElement = document.createElement("li");
	neuesElement.id = "li_" + anzahlElemente;
	var checkBox = document.createElement("input");
	checkBox.type = "checkbox"; // "type="checkbox" zum input-Feld hinzufügen
	checkBox.id = "checkbox_" + anzahlElemente;
	checkBox.onclick = aufgabeErledigen;
	
	var label = document.createElement("label");
	label.id = "element_" + anzahlElemente;
	label.htmlFor = "checkbox_" + anzahlElemente;
	label.innerText = neueAufgabeText;

	var btnEdit = document.createElement("button");
	btnEdit.id = "btnEdit_" + anzahlElemente;
	btnEdit.onclick = editierenElement;
	btnEdit.innerText = "Edit";

	var btnLoeschen = document.createElement("button");
	btnLoeschen.id = "btnLoeschen_" + anzahlElemente;
	btnLoeschen.onclick = loeschenElement;
	btnLoeschen.innerText = "Löschen";

	
	neuesElement.appendChild(checkBox)
	neuesElement.appendChild(label)
	neuesElement.appendChild(btnEdit)
	neuesElement.appendChild(btnLoeschen)
	
	//neuesElement.innerText = neueAufgabeText;
	
	//var liste = document.getElementById("aufgaben_liste");
	liste.appendChild(neuesElement);
}

var anzahlElemente = 0;
var neueAufgabeInput = document.getElementById("neueAufgabeInput");
	neueAufgabeInput.focus();

var btnNeueAufgabe = document.getElementById("btnNeueAufgabe");
btnNeueAufgabe.onclick = function () {
	
	var neueAufgabeText = neueAufgabeInput.value;
	
	// Wenn benutzer nichts eingegeben hat oder auf "Abbechen" geklickt hat:
	if(neueAufgabeText == false || neueAufgabeText == "" || neueAufgabeText == " ") {
		return false;
	}
	neueAufgabeHinzufuegen(document.getElementById("aufgaben_liste"), neueAufgabeText);
	
	neueAufgabeInput.focus();
	neueAufgabeInput.value = "";
}